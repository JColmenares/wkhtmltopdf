# WKHTMLTOPDF

# Para Debian 8 (Jessie amd64) versión 0.12.5 .

Se debe posicionar en una carpeta donde se quiera bajar el archivo

- cd Directorio

Se clona el repositorio

- git clone https://gitlab.com/JColmenares/wkhtmltopdf.git

Se ingresa a la carpeta creada

- cd wkhtmltopdf

Se realiza la instalación 

- sudo dpkg -i wkhtmltox_0.12.5-1.jessie_amd64.deb

- sudo apt-get install -f

Y para finalizar se recomienda realizar una actualización General 

- sudo apt-get update && sudo apt-get upgrade && sudo apt upgrade


# Para Ubuntu ultima versión

Se actualiza el sistema

- sudo apt-get update

Se instala la libreria

- sudo apt-get install xvfb

Se instala Wkhtmltopdf desde el repositorio de ubuntu

- sudo apt-get install wkhtmltopdf